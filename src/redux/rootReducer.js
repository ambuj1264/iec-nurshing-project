import { combineReducers } from "redux";
import {ticketDetailsReducer,loginTypeReducer} from "./reducer"
const rootReducer = combineReducers({
  ticketDetailsReducer, // Rename the reducer key if needed
  loginTypeReducer
});

export default rootReducer;
