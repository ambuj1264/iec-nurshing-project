import { LOGIN_ACTION_TYPE, TICKET_DETAILS } from "./constants"

export const ticketDetails = (data)=>{
    return {
      type: TICKET_DETAILS,
      payload: data
    }
}
export const loginTypeAction = (data)=>{
    return {
      type: LOGIN_ACTION_TYPE,
      payload: data
    }
}