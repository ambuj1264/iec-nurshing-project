import {
  EMAIL_STEP_1,
  LOGIN_ACTION_TYPE,
  MOBILE_NUMBER,
  MOBILE_OTP,
  TICKET_DETAILS,
} from "./constants";

const initialReservation = [];

export const ticketDetailsReducer = (state = initialReservation, action) => {
  switch (action.type) {
    case TICKET_DETAILS: {
      return action.payload;
    }
    default:
      return state;
  }
};
export const loginTypeReducer = (state = "", action) => {
  switch (action.type) {
    case LOGIN_ACTION_TYPE:
      {switch (action.payload.type) {
        case MOBILE_NUMBER:
          return {type: MOBILE_NUMBER, data:action.payload.data};
        case MOBILE_OTP:
          return {type: MOBILE_OTP, data:action.payload.data};
        case EMAIL_STEP_1:
          return {type: EMAIL_STEP_1, data:action.payload.data};
        default:
          return state;
      }}
    default:
      return state;
  }
};
