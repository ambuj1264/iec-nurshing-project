import React, { useState, useCallback } from "react";

export const useApi = apiService => {
  const [responseData, setResponseData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  let callApi;

  callApi = useCallback(
    (...input) => {
      setIsLoading(true);
      setResponseData(null);
      setError(null);

      apiService(...input)
        .then(response => {
          setResponseData(response);
          setError(null);
        })
        .catch(error => {
          setResponseData(null);
          setError(error);
        })
        .finally(() => {
          setIsLoading(false);
        });
    },
    [apiService]
  );

  return { data: responseData, isLoading, error, callApi };
};
