import React from 'react';
import Layout from '../../../components/layout';
import {
    VStack,
    Stack,
    Box,
} from '@chakra-ui/react'

import EventDetails from '../../../components/details/events-details';

const EventsPage = () => {
    return <VStack>
        <Layout showBanner={false}>
           <Stack>
                <EventDetails />
           </Stack>
        </Layout>
    </VStack>
}

export default EventsPage;