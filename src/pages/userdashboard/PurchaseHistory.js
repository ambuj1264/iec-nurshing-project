import React from 'react'
import UserDashboardLayout, { Headername } from '../../components/userdashboard/UserDashboardLayout'

const PurchaseHistory = () => {
  return (
    <UserDashboardLayout>
      <Headername>Purchase History</Headername>
      PurchaseHistory
    </UserDashboardLayout>
  )
}

export default PurchaseHistory