import React, { useState } from "react";
import styled from "styled-components";
import UserDashboardLayout from "../../components/userdashboard/UserDashboardLayout";
import ElderyEvent from "../../components/userdashboard/elderydetail/ElderyEvent";
import { ArrowIcon } from "../../utils/SVGIcons/SVG";
import EditEldery from "../../components/userdashboard/elderydetail/EditEldery";
import AlertModal from "../../components/Modal/AlertModal";

const ElderlyDetail = () => {
  const [isDetail, setDetail] = useState(false);
  const [isEdit,setIsEdit]=useState(false);
  const [isDelete,setIsDelete]=useState(false)
  const handleOpenDetail = () => {
    setDetail(true)
  }
  const handleCloseDetail = () => {
    setDetail(false)
  }
  const onDeleteClose=()=>{
    setIsDelete(false)
  }
  return (
    <UserDashboardLayout>
      <HeadingWrapper onClick={handleCloseDetail}>
        {isDetail && <ArrowIcon width="13" height="12" />}
        <Headername  > Elderly Detail</Headername>
      </HeadingWrapper>
      <SavedEldelyWrapper>
        {!isDetail ? <><SavedEldely>Saved Elderly</SavedEldely>
          {elderlyData?.map((item) => (<CardWrapper>
            <div>
              <ContectDetail onClick={handleOpenDetail}>
                <Uname>{item.name}</Uname>
                <UNumber>{item.number}</UNumber>
                <UEmail>{item.email}</UEmail>
              </ContectDetail>
              <ActionButton>
                <ActBtn onClick={()=>{setIsEdit(true)}}>Edit</ActBtn>
                <ActBtn onClick={()=>setIsDelete(true)}>Delete</ActBtn>
              </ActionButton></div>
            <LeftContent>
              <EventNumber>{item.eventno}</EventNumber>
              <Event>Events</Event>
            </LeftContent>
          </CardWrapper>))}</>
          : <ElderyEvent />
        }      </SavedEldelyWrapper>
        <EditEldery open={isEdit} onclose={setIsEdit}/>
        <AlertModal open={isDelete} onclose={onDeleteClose} heading="Are you sure you want to delete this address?"/>
    </UserDashboardLayout>
  )
};

export default ElderlyDetail;

const HeadingWrapper = styled.div`
display:flex;
align-items:center;
gap:10px;
cursor:pointer
`
const Headername = styled.h1`
color: #333;
font-family: Inter;
font-size: 16px;
font-weight: 600;
line-height: 28px; 

`

const SavedEldelyWrapper = styled.div`
padding:30px 0
`

const SavedEldely = styled.h3`
color: #666;
font-family: Inter;
font-size: 12px;
font-weight: 500;
line-height: 20px;
margin-bottom:5px
`;

const CardWrapper = styled.div`
 display:flex;
 justify-content:space-between;
 align-items:start;
 padding:20px;
 background:white;
 margin-bottom:10px
`;

const ContectDetail = styled.div`cursor:pointer`;

const Uname = styled.h1`
color: #333;
font-size: 18px;
font-weight: 700;
`

const commontStyle = `
color: #666;
font-size: 14px;
font-weight: 400;
`

const UNumber = styled.h1`
${commontStyle}
`
const UEmail = styled.h1`
${commontStyle}

`;

const ActionButton = styled.div`
display:flex;
align-items:center;
gap:10px;
margin-top:10px
`
const ActBtn = styled.button`
color:#0A84FF;
font-size: 14px;
font-weight: 400;
line-height: 20px;
`
const LeftContent = styled.div``;

const EventNumber = styled.h1`
color: #333;
font-size: 28px;
font-weight: 500;

`
const Event = styled.div`
color: #666;
font-size: 12px;
font-weight: 400;
`

const elderlyData = [
  {
    id: 1,
    name: "Abdul Rashid Mansuri",
    number: "+91 9930391551",
    email: "abdulrashid@gmail.com",
    eventno: 10
  },
  {
    id: 2,
    name: "Parveen Abdul Rashid Mansuri",
    number: "+91 9930391551",
    email: "parveenarmansuri02912@gmail.com",
    eventno: 10
  },
  {
    id: 3,
    name: "Parveen Abdul Rashid Mansuri",
    number: "+91 9930391551",
    email: "parveenarmansuri02912@gmail.com",
    eventno: 10
  }

]

