// pages/dashboard/index.js
import React from 'react';
import { useRouter } from 'next/router';
import ElderlyDetail from './elderlyDetail';

const DashboardIndex = () => {
  const router = useRouter();
  React.useEffect(() => {
    router.push('/userdashboard/elderlyDetail');
  }, [router]);

  return <ElderlyDetail/>;
};

export default DashboardIndex;
