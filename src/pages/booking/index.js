import React from "react";
import { Stack} from "@chakra-ui/react";
import Layout from "../../components/layout";
import CommonStepper from "../../components/list/ui/stepper/Stepper";
import Header from "../../components/booking/events/header";
import { ArrowIcon } from "../../utils/SVGIcons/SVG";
import { eventbookingsteps } from "../../utils/constant";
import ProceedButton from "../../components/common/ProceedSection/ProceedButton";

const Booking = () => {
    return (
        <>
            <Layout showBanner={false}>
                <Stack maxWidth={"776px"} width={"100%"} style={{ border: "1px solid #EEEEEE", backgroundColor:"white" }}>
                    <Header BackIcon={ArrowIcon} title="Yoga & Meditations : Theme Based senior (35-80 years)" startTime="at 6:00pm" venue="OneNest: Mumbai" />
                    <CommonStepper steps={eventbookingsteps} activeStep={0} />
                    <ProceedButton/>
                </Stack>
            </Layout>

        </>
    )
};

export default Booking;










