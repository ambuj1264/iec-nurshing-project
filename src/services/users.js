import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { get } from "./request";

const users = createApi({
  reducerPath: "users",
  baseQuery: fetchBaseQuery({baseUrl: `${process.env.NEXT_PUBLIC_API_ENDPOINT}`}),
  tagTypes: ["User"],

  endpoints: builder => ({
    getProfileByUsername: builder.query({
      query: username => `/api/user/${username}`,
      providesTags: ["User"]
    }),

    verifyEmailByToken: builder.query({
      query: token => `/auth/validateEmailVerification/${token}`,
      providesTags: ["User"]
    }),


    createUser: builder.mutation({
      query: data => ({
        url: `/api/auth/signup`,
        method: "POST",
        body: { ...data }
      }),
      invalidatesTags: ["User"]
    }),

    updateProfile: builder.mutation({
      query: ({ username, ...data }) => ({
        url: `/api/user/${username}`,
        method: "PUT",
        body: { ...data }
      }),
      invalidatesTags: ["User"]
    }),

    resetPassword: builder.mutation({
      query: data => ({
        url: `/auth/reset-password`,
        method: "PUT",
        body: { ...data }
      }),
      invalidatesTags: ["User"]
    }),

    updateLayout: builder.mutation({
      query: data => ({
        url: `/api/user/${data.id}/update-layout`,
        method: "POST",
        body: { ...data }
      }),
      invalidatesTags: ["User"]
    }),

    forgotPassword: builder.mutation({
      query: data => ({
        url: `/auth/forgot-password`,
        method: "POST",
        body: { ...data }
      }),
      invalidatesTags: ["User"]
    })
  })
});

export const {
  useCreateUserMutation,
  useVerifyEmailByTokenQuery,
  useVerifyTokenMutation,
  useUpdateLayoutMutation,
  useUpdateProfileMutation,
  useResetPasswordMutation,
  useForgotPasswordMutation,
  useGetProfileByUsernameQuery
} = users;

export default users;

export const getUser = (username, baseUrl = "") => {
  return get(`${baseUrl}/api/user/${username}`);
};
