import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const ticketTypes = createApi({
  reducerPath: "ticketTypes",

  baseQuery: fetchBaseQuery({baseUrl: `${process.env.NEXT_PUBLIC_API}/api/ticket-types`}),
  tagTypes: ["TicketType"],

  endpoints: (builder) => ({
    listTicketTypes: builder.query({
      query: (data) => `/${data.eventCode}/list?isOnline=${data.isOnline || true}&isWeb=true`,
      providesTags: ["TicketType"],
    }),

    ticketTypeDetails: builder.query({
      query: (id) => `/${id}`,
      providesTags: ["TicketType"],
    }),
  }),
});

export const {
  useListTicketTypesQuery,
  useTicketTypeDetailsQuery,
} = ticketTypes;

export default ticketTypes;
