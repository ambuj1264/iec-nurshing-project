import React, {useState} from 'react';
import { useRouter } from 'next/router'
import { 
    Box,
    Stack
} from '@chakra-ui/react';

import Container from '../common/container';
import Header from './header';
import EventBookingSteps from '../common/stepper';
import { useEventDetailsQuery } from '../../../services/events';
// import { useListTicketTypesQuery } from '../../.../services/ticketTypes';
import {useListTicketTypesQuery } from '../../../services/ticketTypes'
import * as moment from 'moment';

const EventsBooking = () => {
    const router = useRouter();
    const eventCode = router?.query?.eventCode;
    console.log('eventCode', eventCode)
    
    const {
        isLoading: isEventDetailsLoading,
        data: event,
        isError: isEventDetailsError,
    } = useEventDetailsQuery(eventCode);

    const {
        isLoading,
        data: ticketTypes,
        isError,
    } = useListTicketTypesQuery({eventCode: eventCode});

    const dateString = moment(event?.startDateTime).format('ddd DD, MMM')
    const timeString = moment(event?.startDateTime).format('HH:mm A')

    
    return <Container>
        <Header title={event?.name} startTime={timeString} venue={event?.onGroudVenueDetails}/>
        <EventBookingSteps ticketTypesDetails={ticketTypes}/>
    </Container>
}

export default EventsBooking;