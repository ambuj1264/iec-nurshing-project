import React, { useState} from 'react';
import { 
    Box,
    Stack,
    Heading
} from '@chakra-ui/react';

const Container = ({children}) => {
    return <Box maxWidth={'3xl'} borderWidth={'1px'} borderColor={'#EEEEEE'} borderStyle={'solid'}>
        {children}
    </Box>
}

export default Container;