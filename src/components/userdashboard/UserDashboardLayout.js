// components/UserDashboardLayout.js
import React from 'react';

import { Container } from "@chakra-ui/react";
import styled from "styled-components";
import Sidebar from './Sidebar';
import Layout from '../layout';

const UserDashboardLayout = ({ children }) => {
  return (
    <div>
      <Layout showBanner={false}>
        <Container maxW={"7xl"}>
          <Wrapper >
            <Sidebar/>
            <SideContent>
              {children}
              </SideContent>
              </Wrapper>
              </Container>
              </Layout>
    </div>
  );
};

export default UserDashboardLayout;

const Wrapper = styled.div`
display:flex;
background:white;
border:1px solid #f3f3f3;
min-height:975px
`

const SideContent = styled.div`
background:#F3F3F3;
margin:25px;
padding:20px;
flex:72%
`


export const Headername = styled.h1`
color: #333;
font-family: Inter;
font-size: 16px;
font-weight: 600;
line-height: 28px; /* 175% */

`