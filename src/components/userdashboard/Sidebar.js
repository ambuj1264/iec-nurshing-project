import { Box, VStack, Collapse, Avatar } from '@chakra-ui/react';
import { AccountSetting, CarePointIcon, DropDownArrow, HostEventIcon, LoginHeaderIcon, PurchesHistoryIcon, ReviewIcon, SupportIcon } from '../../utils/SVGIcons/SVG';
import styled from 'styled-components';
import { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

const SidebarItem = ({ item }) => {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();

  const handleToggle = () => {
    setIsOpen(!isOpen);
  };
  const isActive = router.pathname === item.route;

  return (
    <Box width="100%" bg={isActive ? '#E8F9FE' : 'transparent'}
      color={isActive ? '#FFF' : '#333'}>
      <Link href={item.route || '#'}>

        <DropDownItem onClick={handleToggle} isActive={isActive}>
          {item.icon}
          <Content>
            <DropDownHeading>{item.title}</DropDownHeading>
            <DropDownDetail>{item.detail}</DropDownDetail>
          </Content>
          {item.items && <DropDownArrow />
          }      </DropDownItem>
      </Link>
      <Collapse in={isOpen}>
        <VStack align="start" pl={14} P={0}>
          {item.items && item.items.map((subItem, index) => (
            <SidebarItem key={index} item={subItem} />
          ))}
        </VStack>
      </Collapse>
      <hr />
    </Box>
  );
};

const Sidebar = ({ data }) => {
  return (
    <SidebarWrapper>
      <ProfileWrapper>
        <AvtarWrapper>
          <Avatar
            size='lg'
            name='Prosper Otemuyiwa'
            src='https://bit.ly/prosper-baba'
          />    </AvtarWrapper>    <DetailWrapper>
          <Name>Hi Wasim!</Name>
          <Number>+91 8108088722 </Number>
          <Editbtn>Edit profile</Editbtn>
        </DetailWrapper>
      </ProfileWrapper>
      <BookingDetails>
        <BookingNumber>4</BookingNumber>
        <BookingText>Your Bookings int he last year <br /><Span>Events</Span></BookingText>
      </BookingDetails>
      <hr />
      <DropDownWrapper>
        {sidebarData?.map((item, index) => (
          <SidebarItem key={index} item={item} />
        ))}
      </DropDownWrapper>
      <BottomBtnWrapper>
        <BottomBtn>Share</BottomBtn>
        <BottomBtn>Rate Us</BottomBtn>
        <BottomBtn><LoginHeaderIcon width={62} height={18} /></BottomBtn>
      </BottomBtnWrapper>
      <LogoutBtn>Log Out</LogoutBtn>
    </SidebarWrapper>
  );
};

export default Sidebar;

const AvtarWrapper = styled.div`
padding:10px;
border-radius:50%;
background:white;

`

const SidebarWrapper = styled.div`flex:25%;
border-right:1px solid #EEEDFB;
height:100%
`;
const ProfileWrapper = styled.div`
display:flex;
align-items:center;
background:#13B8E9;
padding:20px;
gap:15px
`

const DetailWrapper = styled.div``;

const Name = styled.h1`
color: #FFF;
font-size: 18px;
font-weight: 700;
line-height: 28px; /* 155.556% */
margin-bottom:5px
`;

const Number = styled.h1`
color: #FFF;
font-size: 14px;
font-weight: 500;
line-height: 100%; /* 14px */
margin-bottom:5px
`;

const Editbtn = styled.button`color:white`

const BookingDetails = styled.div`
display:flex;
gap:15px;
align-items:center;
padding:20px
`

const BookingNumber = styled.div`
border-radius:50%;
background:#4A4D5E;
width:32px;
height:32px;
color: #FFF;
font-size: 20px;
font-weight: 600;
line-height: 15px; /* 75% */
display:grid;
place-content:center
`

const BookingText = styled.h1`
color: #333;
font-size: 12px;
font-weight: 400;
line-height: 15px; /* 125% */
`

const Span = styled.span`
dislay:block;
color: #333;
font-size: 12px;
font-weight: 700;
line-height: 15px; /* 125% */
`

const DropDownWrapper = styled.div`
display:flex;
flex-direction:column;
`

const DropDownItem = styled.div`
display:flex;
align-items:center;
padding:15px 25px;
justify-content:space-between;
background-color: ${props => (props.isActive ? '#E8F9FE' : 'transparent')};
color: ${props => (props.isActive ? '#FFF' : '#333')};

`

const Content = styled.div`
`;

const DropDownHeading = styled.h1`
color: #333;
font-family: Inter;
font-size: 12px;
font-weight: 600;
line-height: 14px; /* 116.667% */
`;

const DropDownDetail = styled.p`
color: #666;
font-family: Inter;
font-size: 10px;
font-weight: 400;
line-height: 14px; /* 140% */
`

const BottomBtnWrapper = styled.div`
display:flex;
align-items:center;
justify-content:space-between;
padding:20px;
`

const BottomBtn = styled.button`
border-radius: 4px;
border: 1px solid #EEE;
background: #FFF;
padding:3px 10px
`

const LogoutBtn = styled.div`
color: #0A84FF;
font-size: 14px;
font-weight: 600;
padding:0 15px 10px 15px;
cursor:pointer
`;

const sidebarData = [
  {
    title: 'Purchase History',
    detail: 'View all your bookings & purchases',
    route: "/userdashboard/PurchaseHistory",
    icon: <PurchesHistoryIcon />,
    items: [
      { title: 'Purchase History', route: "/userdashboard/PurchaseHistory" },
      {
        title: 'Child Item',
        items: [
          { title: 'Purchase History 1', route: "/userdashboard/PurchaseHistory1" },
          { title: 'Purchase History 2', route: "/userdashboard/PurchaseHistory2" },
        ],
      },
    ],
  },
  {
    title: 'Elderly Details',
    detail: 'View all your bookings & purchases',
    icon: <PurchesHistoryIcon />,
    route: "/userdashboard/elderlyDetail",
    items: [
      { title: 'Elderly Details', route: "/userdashboard/elderlyDetail" },
    ],
  },
  {
    title: 'Help & Support',
    detail: 'View all your bookings & purchases',
    icon: <SupportIcon />,
    items: [
      { title: 'Help & Support', route: "/userdashboard/elderlyDetail" },
    ],
  },
  {
    title: 'Accounts & Settings',
    detail: 'View all your bookings & purchases',
    icon: <AccountSetting />,
    items: [
      { title: 'Accounts & Settings', route: "/userdashboard/elderlyDetail" },
    ],
  },
  {
    title: 'Care Points',
    detail: 'View all your bookings & purchases',
    icon: <CarePointIcon />,
    items: [
      { title: 'Care Points', route: "/userdashboard/elderlyDetail" },
    ],
  },
  {
    title: 'Review',
    detail: 'View all your bookings & purchases',
    icon: <ReviewIcon />,
    items: [
      { title: 'Review', route: "/userdashboard/Review" },
    ],
  },
  {
    title: 'Host your Events',
    detail: 'View all your bookings & purchases',
    icon: <HostEventIcon />,
    items: [
      { title: 'Host your Events', route: "/userdashboard/Review" },
    ],
  },
];