import React from 'react';
import ModalComponent from '../../list/ui/modals/ModalComponent';
import styled from 'styled-components';
import FormikInput from '../../list/ui/input/FormikInput';
import { buttonStyle, inputStyle } from '../../eventbooking/tabsStep/Yourself';
import { Button } from '@chakra-ui/react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    fullname: Yup.string()
        .min(3, 'Name must be at least 3 characters')
        .required('Name is required'),
    email: Yup.string()
        .email('Invalid email address')
        .required('Email is required'),
    phoneNumber: Yup.string()
        .matches(/^[0-9]+$/, 'Phone number must be numeric')
        .min(10, 'Phone number must be at least 10 digits')
        .max(15, 'Phone number must not exceed 15 digits')
        .required('Phone number is required'),
});

const initialValues = {
    fullname: '',
    email: '',
    phoneNumber: '',
};

const onSubmit = (values) => {
    console.log('Form submitted with values:', values);
};

const EditEldery = ({ open, onclose }) => {
    return (
        <ModalComponent isCancelIcon isOpen={open} onClose={() => onclose(false)}>
            <Heading>Edit Elderly</Heading>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                <StyledForm>
                    <InputContainer>
                        <FormikInput
                            id="fullname"
                            style={inputStyle}
                            labelStyle={labelStyle}
                            label="Full Name*"
                            name="fullname"
                            placeholder="Enter your full name"
                        />
                    </InputContainer>
                    <InputContainer>
                        <FormikInput
                            id="phoneNumber"
                            style={inputStyle}
                            labelStyle={labelStyle}
                            label="Mobile Number*"
                            name="phoneNumber"
                            placeholder="Enter your mobile number*"
                        />
                    </InputContainer>
                    <InputContainer>
                        <FormikInput
                            id="email"
                            style={inputStyle}
                            labelStyle={labelStyle}
                            label="Email Address"
                            name="email"
                            placeholder="Enter your email address"
                        />
                    </InputContainer>
                    <ButtonWapper>
                        <Button
                            type="submit"
                            style={{ ...buttonStyle, background: '#0A84FF', color: '#fff' }}
                            _hover={{
                                bg: 'transparent',
                                borderColor: '#28BCA5',
                                color: '#28BCA5',
                            }}
                        >
                            Submit
                        </Button>
                    </ButtonWapper>
                </StyledForm>
            </Formik>
        </ModalComponent>
    );
};

export default EditEldery;

const InputContainer = styled.div`
  margin-bottom: 25px;
`;

const ButtonWapper = styled.div``;

const labelStyle = {
    color: '#666',
    fontFamily: 'Inter',
    fontSize: '12px',
    fontWeight: 500,
    margin: 0,
};

const Heading = styled.h1`
  color: #333;
  font-size: 24px;
  font-weight: 700;
  padding: 10px 0;
`;

const StyledForm = styled(Form)`
  margin: 10px 0;
`;
