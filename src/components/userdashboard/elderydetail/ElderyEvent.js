import React from 'react'
import EventCards from './EventCards'
import styled from 'styled-components';

const ElderyEvent = () => {
  return (
    <Wrapper>
        {
            [...Array(10)].map((item)=>(
              <CardWrapper>
                <EventCards/>
                </CardWrapper>
            ))
        }
    </Wrapper>
  )
}

export default ElderyEvent;

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
  justify-content:center

`;

const CardWrapper=styled.div`
flex: 0 0 calc(50% - 20px); 
`