import { Divider } from "@chakra-ui/react";
import Image from "next/image";
import React from "react";
import styled from "styled-components";

const EventCards = () => {
  return (
    <EventCardsWrapper>
    <StyledDate>Mon, 01 Dec 2021</StyledDate>
    <Card>
      <CardHeader>
        <CardHeaderRight>
          <StyledImg src="/images/other/yoga.png" width={100} height={100} alt="yoga"/>
          <div>
            <HeaderTitle>Yoga & Meditation : Theme Based (35-80 years)</HeaderTitle>
            <EventDetail>04:00PM</EventDetail>
            <EventDetail>OneNest: Mumbai</EventDetail>
          </div>
        </CardHeaderRight>
        <TiketType>M-Ticket</TiketType>
      </CardHeader>
      <CardBottom>
        <CardHeaderLeft>
          <Span>
            MON<br/>
            09<br/>
            DEC
          </Span>
        </CardHeaderLeft>
        <Divider orientation='vertical' h={"auto"} />
        <CardBottomRight>
          <Seat>Seat</Seat>
          <ClassName>Silver,Class</ClassName>
        </CardBottomRight>
      </CardBottom>
    </Card>
    </EventCardsWrapper>
  );
};

export default EventCards

const EventCardsWrapper=styled.div``

const StyledDate=styled.h1`
color: #666;
font-family: Inter;
font-size: 12px;
font-weight: 500;
line-height: 20px; 
`

const Card = styled.div`
  width: auto;
  border-radius: 5px;
  background: white;
  overflow: hidden;
`;

const CardHeader = styled.div`
  height: 100%;
  border-bottom: 2px solid #EEE;
  border-radius: 10px 10px 0 0;
  position: relative;
  display:flex;
  justify-content:space-between;
  gap:20px;
  padding:20px;
  padding-right:10px;
  &:before,
  &:after {
    content: '';
    position: absolute;
    width: 24px;
    height: 24px;
    background: #F3F3F3;
    border-radius: 100%;
    bottom: -12px;
    border: 1px solid #F3F3F3;
    box-sizing: border-box;
  }

  &:before {
    left: -13px;
  }

  &:after {
    right: -13px;
  }
`;

const CardBottom = styled.div`
  min-height: 50px;
  height:100%;
  display:flex;
  gap:25px;
  padding:10px 40px;
  border-top: none;
  border-radius: 0 0 10px 10px;
`;
const StyledImg=styled(Image)`
 width:auto;
 height:100%;
`

const CardHeaderRight=styled.div`
 display:flex;
 justify-content:space-start;
 align-items:start;
 gap:15px
`
const HeaderTitle=styled.h1`
color: #333;
font-family: Inter;
font-size: 14px;
font-weight: 600;
line-height: 20px;
max-width:220px;
margin-bottom:5px
`


const EventDetail=styled.h2`
color: #666;
font-family: Inter;
font-size: 12px;
font-weight: 500;
line-height: 20px;
`

const TiketType=styled.h1`
color: #666;
font-family: Inter;
font-size: 12px;
font-weight: 500;
line-height: 20px;
`
const CardHeaderLeft=styled.div``;

const Span=styled.span`
color: #666;
font-family: Inter;
font-size: 16px;
font-weight: 600;
line-height: 20px; 
letter-spacing: 1px;
`

const CardBottomRight=styled.div`
`;

const Seat=styled.h1`
color: #666;
font-family: Inter;
font-size: 12px;
font-weight: 400;
line-height: 20px;
`;

const ClassName=styled.h1`
color: #666;
font-family: Inter;
font-size: 14px;
font-weight: 500;
line-height: 20px; 
`