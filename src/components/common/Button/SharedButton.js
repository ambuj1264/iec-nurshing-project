import styled from 'styled-components';

// Outlined Button
export const OutlinedButton = styled.button`
  border: 2px solid #D8D8D8;
  color: #0A84FF;
  background: transparent;
  font-size: 16px;
  font-weight: 700;
  line-height: 19px;
  padding: 13px 20px;
  cursor: pointer;
  border-radius: 2px;
  width:100%;
  text-align:center;
  transition: background-color 0.3s ease, color 0.3s ease;

  &:hover {
    background-color: #0A84FF;
    color: #fff;
  }
`;

// Background Button
export const BackgroundButton = styled.button`
  border: none;
  color: #fff;
  background-color: #0A84FF;
  font-size: 16px;
  font-weight: 700;
  line-height: 19px;
  padding: 13px 20px;
  cursor: pointer;
  border-radius: 2px;
  width:100%;
  text-align:center;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #0073e6; 
  }
`;
