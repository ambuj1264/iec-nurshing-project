import React from "react";
import { Badge, Box, Button, Divider, Flex, Image, Text } from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
import * as moment from 'moment';
import { slugifyUrl } from '../../../libs/utils'
export default function EventCard({event}){
  const property = {
    imageUrl: "https://bit.ly/2Z4KKcF",
    imageAlt: "Rear view of modern home with pool",
    beds: 3,
    baths: 2,
    title: "Modern home in city center in the heart of historic Los Angeles",
    formattedPrice: "$1,900.00",
    reviewCount: 34,
    rating: 4,
  };
  console.log('props in event card', event)
  const dateString = moment(event?.startDateTime).format('ddd DD, MMM')
  const timeString = moment(event?.startDateTime).format('HH:mm A')
  return (
    <Flex
    //   p={50}
      w="full"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        bg="white"
        _dark={{ bg: "gray.800" }}
        maxW="sm"
        borderWidth="1px"
        rounded="lg"
        shadow="lg"
      >
        <Box position={'relative'}>
            <Image
                src={event?.cardBannerUrl ? event?.cardBannerUrl : property.imageUrl}
                alt={property.imageAlt}
                roundedTop="lg"
            />
            <Box position={'absolute'} bottom={'-5px'} px={2}>
                {event?.genere && event?.genere.map(genere => {
                  return <Badge rounded="3" fontSize={'10px'} px={3} py={1} colorScheme="teal">
                    {genere.value}
                  </Badge>
                })}
                
            </Box>
            
        </Box>
       

        <Box p={3}>
          <Box display="flex" alignItems="baseline">
           
            <Box
              color="gray.500"
              fontWeight="semibold"
              letterSpacing="wide"
              fontSize="xs"
              textTransform="uppercase"
              ml="0"
            >
               {dateString} | {timeString}
            </Box>
          </Box>

          <Text
            mt="1"
            fontWeight="semibold"
            as="h4"
            lineHeight="tight"
            noOfLines={1}
            width={'100%'}
            maxW={'200px'}
          >
            {event.name}
          </Text>

          <Box mb={4}>
            {property.formattedPrice}
            <Box as="span" color="gray.600" fontSize="sm">
              / wk
            </Box>
          </Box>
          <Divider/>
          <Box display="flex" mt="2" alignItems="center" justifyContent={'flex-end'} >
            <Box as="span" ml="2" color="gray.600" fontSize="sm">
                
            </Box>
            <Box as="span" ml="2" color="gray.600"fontSize="sm">
               <Button as={'a'} href={`/events/${slugifyUrl(event?.name)}/${event?.code}`}>Book Now</Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

