import React from 'react';

import {
  SimpleGrid,
  VStack,
  Heading,
  Text,
  Box,  
  GridItem
} from '@chakra-ui/react';

import EventCard from '../common/cards/event';
import { useListEventQuery } from '../../services/events'

const FeaturedEvents = () => {
    
}

const EventListing = () => { 
    const {
        isLoading,
        data: events,
        isError,
    } = useListEventQuery();
    console.log('data', events)
    return <>
        <VStack maxWidth={'7xl'} flexWrap={'wrap'}>
            <SimpleGrid columns={{md: 4, base: 1}} gap={{md:5, base: 2}}>
                {events && events.map((event,index) => {
                    return   <GridItem>
                    <EventCard key={index}  event={event}/>
               </GridItem>
                })}
                 
            </SimpleGrid> 
        </VStack>
    </>
}

export default EventListing;