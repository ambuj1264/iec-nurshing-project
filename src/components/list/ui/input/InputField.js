// InputField.js
import React from 'react';
import { Input, FormLabel, FormControl, FormHelperText } from '@chakra-ui/react';

const maindivStyle = {
  width: "100%"
}


const InputField = ({ label, labelStyle, formControlStyle = maindivStyle, placeholder, helperText, ...rest }) => {
  return (
    <FormControl style={formControlStyle}>
      {label && <FormLabel style={labelStyle}>{label}</FormLabel>}
      <Input variant="flushed" placeholder={placeholder} {...rest} />
      {helperText && <FormHelperText color={"red"}>{helperText}</FormHelperText>}
    </FormControl>
  );
};

export default InputField;
