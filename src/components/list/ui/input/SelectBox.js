// SelectBox.js
import React from 'react';
import { Select, FormLabel, FormControl, FormHelperText } from '@chakra-ui/react';

const SelectBox = ({ label, maxWidth="auto", options, helperText, ...rest }) => {
  return (
    <FormControl maxWidth={maxWidth}>
      {label && <FormLabel>{label}</FormLabel>}
      <Select {...rest}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </Select>
      {helperText && <FormHelperText color="red">{helperText}</FormHelperText>}
    </FormControl>
  );
};

export default SelectBox;
