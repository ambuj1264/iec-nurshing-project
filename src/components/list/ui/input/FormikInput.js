import React from 'react';
import styled from 'styled-components';
import { Input, FormLabel, FormControl, FormHelperText } from '@chakra-ui/react';
import { useField } from 'formik';

const StyledInput = styled(Input)`
  border: none;
  border-radius: 0;
  border-bottom: 2px solid #eee;
  box-shadow:none !important;
  &:focus {
    border-color: gray.400;
  }
`;

const StyledFormControl = styled(FormControl)`
  width: 100%;
`;

const FormikInput = ({ label, labelStyle, formControlStyle, placeholder, helperText, ...rest }) => {
  const [field, meta, helpers] = useField(rest);
  const isFocused = meta.touched && meta.error;

  const handleChange = (e) => {
    helpers.setValue(e.target.value);
    helpers.setTouched(true);
  };

  return (
    <StyledFormControl style={formControlStyle} isInvalid={isFocused}>
      {label && <FormLabel style={labelStyle}>{label}</FormLabel>}
      <StyledInput {...field} placeholder={placeholder} onChange={handleChange} onBlur={() => helpers.setTouched(true)} isFocused={isFocused} {...rest} />
      {isFocused && meta.error && <FormHelperText color={"red"}>{meta.error}</FormHelperText>}
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </StyledFormControl>
  );
};

export default FormikInput;
