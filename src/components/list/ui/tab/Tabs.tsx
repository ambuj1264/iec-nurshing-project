import { Tabs, TabList, Tab, TabPanel, TabPanels } from '@chakra-ui/react';
import React from 'react';

const TabsComponent = ({ tabsData }) => {
  return (
    <Tabs  >
      <TabList>
        {tabsData.map((tab, index) => (
          <Tab _selected={{ color: '#000',borderBottom:"2px solid blue"}} color={"#666"} style={tabStyle} key={index}>{tab.label}</Tab>
        ))}
      </TabList>

      <TabPanels>
        {tabsData.map((tab, index) => (
          <TabPanel px={0} key={index}>
            {tab.content}
          </TabPanel>
        ))}
      </TabPanels>
    </Tabs>
  );
};

export default TabsComponent;


const tabStyle={
fontSize: "16px",
fontWeight: 600,
lineHeight: "19px",
}