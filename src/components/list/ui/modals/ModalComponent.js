// CommonModal.js
import React from 'react';
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter } from '@chakra-ui/react';

const modalStyles = {
  content: {
    borderRadius: 0,
  },
};

const ModalComponent = ({ isOpen, onClose, children, headerComponent, footerComponent, isCancelIcon = true,...rest }) => {
  return (
    <Modal  isOpen={isOpen} onClose={onClose} size="md" {...rest}>
      <ModalOverlay />
      <ModalContent style={modalStyles.content}>
        {headerComponent && <ModalHeader>{headerComponent}</ModalHeader>}
        {isCancelIcon && <ModalCloseButton />}
        <ModalBody>{children}</ModalBody>
        {footerComponent && <ModalFooter>
          {footerComponent}
        </ModalFooter>}
      </ModalContent>
    </Modal>
  );
};

export default ModalComponent;
