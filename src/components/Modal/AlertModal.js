import React from 'react';
import { BackgroundButton, OutlinedButton } from '../common/Button/SharedButton';
import ModalComponent from '../list/ui/modals/ModalComponent';
import styled from 'styled-components';


const AlertModal = ({ open, onclose, heading }) => {
    return (
        <ModalComponent isCancelIcon={false} isOpen={open} onClose={onclose} isCentered size={"lg"}>
            <Heading>{heading}</Heading>
            <ButtonWrapper>
                <OutlinedButton>
                Cancel
                </OutlinedButton>
                <BackgroundButton onClick={onclose}>
                Delete
                </BackgroundButton>
            </ButtonWrapper>
        </ModalComponent>
    );
};

export default AlertModal;

const Heading = styled.h1`
color: #333;
font-family: Inter;
font-size: 24px;
font-weight: 700;
line-height: 32px; 
padding-top:30px
`

const ButtonWrapper=styled.div`
 display:flex;
 align-items:center;
 gap:25px;
 padding:30px 0
`