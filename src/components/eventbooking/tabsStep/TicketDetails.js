import { useFormik } from "formik";
import { VStack, Box, FormLabel, Flex, Button, Checkbox, FormControl } from "@chakra-ui/react";
import InputField from "../../list/ui/input/InputField";
import SelectBox from "../../list/ui/input/SelectBox";
import { validationSchema } from "../../../utils/validation/ValidationSchema";
import { countryoptions } from "../../../utils/constant";
import { useDispatch } from "react-redux";
import { ticketDetails } from "../../../redux/action";
import { useEffect } from "react";


const TicketDetails = () => {
    const dispatch = useDispatch();
    const { values,touched,errors,handleBlur, handleChange, handleSubmit } = useFormik({
        initialValues: { name: "", email: "", contry: "+91", number: "", tickettype: "Bronze" },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            dispatch(ticketDetails(values))
        }
    });
 
    useEffect(() => {
       
        if (Object.keys(errors).length === 0 && Object.keys(touched).length !== 0) {
            dispatch(ticketDetails(values));
        }else{
            dispatch(ticketDetails({}));     
        }
    }, [values, errors, touched, dispatch]);

    return (
        <>
            <VStack align="stretch" mx="auto" my={"10px"}>
                <form onSubmit={handleSubmit}>
                    <Box mb={"15px"}>
                        <InputField style={inputStyle}
                            label="Full Name*"
                            variant="flushed"
                            labelStyle={labelStyle}
                            placeholder="Please enter your name"
                            name="name"
                            type="text"
                            value={values.name}
                            helperText={touched.name && errors.name} 
                            onChange={handleChange}
                            onBlur={handleBlur} 
                             />
                    </Box>
                    <Box mb={"25px"}>
                        <FormLabel style={labelStyle}>Mobile Number*</FormLabel>
                        <Flex gap={"15px"}>
                            <SelectBox   variant="flushed" options={countryoptions} maxWidth="100px" name="contry" style={inputStyle} onChange={handleChange} value={values.contry} helperText={errors.contry} />
                            <InputField style={inputStyle}
                                labelStyle={labelStyle}
                                variant="flushed"
                                placeholder="Please enter number"
                                name="number"
                                type="number"
                                value={values.number}
                                helperText={touched.number && errors.number}
                                onBlur={handleBlur} 
                                onChange={handleChange} />
                        </Flex>
                    </Box>
                    <Box mb={"25px"}>
                        <InputField style={inputStyle}
                            label="Email*"
                            labelStyle={labelStyle}
                            type="email"
                            variant="flushed"
                            placeholder="Please enter your email"
                            name="email"
                            value={values.email}
                            helperText={touched.email && errors.email}
                            onBlur={handleBlur} 
                            onChange={handleChange} />
                        <p style={{
                            paddingTop:"20px",
                            height: "17px",
                            top: "516px",
                            left: "351px",
                            color: "#333333",
                            fontFamily: "Inter",
                            fontSize: "14px",
                            fontWeight: "400",
                            lineHeight: "17px",
                            letterSpacing: "0em",
                            textAlign: "left",
                        }}>This email address will receive the e-tickets</p>

                    </Box>


                </form>

            </VStack>

        </>
    )
}

export default TicketDetails;

export const inputStyle = {
}

const labelStyle = {
    color: "#666",
    fontFamily: "Inter",
    fontSize: "12px",
    fontWeight: 500,
    lineHeight: "19px" /* 158.333% */
}

const buttonStyle = {
    width: "100%",
    color: "#28BCA5",
    bg: "transparent",
    borderColor: "#28BCA5",
    borderRadius: "0px",
    padding: "25px",
    maxHeight: "40px"
}

const checkboxstyle = {
    color: "#666",
    fontFamily: "Inter",
    fontSize: "14px",
    fontWeight: 400,
    textTransform: "capitalize"
}
