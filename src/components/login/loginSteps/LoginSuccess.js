import Image from 'next/image'
import React from 'react'
import SuccessBg from "../../../../public/images/other/SuccessBg.png"
import styled from 'styled-components'
import { SuccessCheckIcon } from '../../../utils/SVGIcons/SVG'
const LoginSuccess = () => {
  return (
    <SuccesWrapper>
      <Image src={SuccessBg} width={296} height={205} alt='successimg' />
      <IconWrapper>
        <SuccessCheckIcon />
      </IconWrapper>
      <Message>Registration Completed Successfully</Message>
    </SuccesWrapper>
  )
}

export default LoginSuccess;


const SuccesWrapper = styled.div`
display: flex;
flex-direction: column;
align-items: center;
`

const Message = styled.h1`
color: #333;
text-align: center;
font-size: 20px;
font-weight: 800;
padding:30px 50px 
`
const IconWrapper = styled.div`
margin-top: -17px
`