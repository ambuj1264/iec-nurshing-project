import React, { useState } from 'react';
import { FooterComponent, HeaderComponent } from '../Login';
import styled from 'styled-components';

const Verification = ({ onLoginStep, formikProps,isEmail }) => {
  const [otp, setOtp] = useState(['', '', '', '']);
  const {errors,values,isValid={isValid},handleChange}=formikProps;
  const handleInputChange = (index, e) => {
    const {value}=e.target
    const newOtp = [...otp];
    newOtp[index] = value.replace(/\D/g, '');
    setOtp(newOtp);
    handleChange(e)  
  };
console.log(errors,values,otp)
  const title=isEmail?"Verify your Email":"Verify your Mobile Number";
  const subTitle=isEmail?`Enter OTP sent to ${values?.email}`:`Enter OTP sent to ${values?.number}`
  return (
    <Wrapper>
      <HeaderComponent title={title} subTitle={subTitle} />
      <OtpWrapper>
        {[1, 2, 3, 4].map((index) => (
          <OtpInputField
            key={index}
            type="text"
            maxLength="1"
            name={`otp${index}`}
            value={values[index]}
            onChange={(e) => handleInputChange(index, e)}
          />
        ))}
      </OtpWrapper>
      {(errors.otp1 || errors.otp2 || errors.otp3 || errors.otp4) && <ErrorText>All fields are required
        </ErrorText>}
      <BottomText>Expect OTP in 21 seconds</BottomText>
      <FooterComponent text="CONTINUE" isValid={isValid} onclick={onLoginStep} />
    </Wrapper>
  );
};

export default Verification;

const Wrapper = styled.div``;

const OtpWrapper = styled.div`
  position: relative;
`;

const BottomText = styled.div`
  position: absolute;
  bottom: 75px;
  left: 50%;
  right: 0;
  text-align: center;
  transform: translate(-50%, 0px);
`;

const OtpInputField = styled.input`
  width: 50px;
  height: 50px;
  text-align: center;
  font-size: 16px;
  margin: 5px;
  border: 1px solid #ccc;
  border-radius: 5px;
  outline: none;
  &:focus {
    border-color: #007bff;
    box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
  }
`;

const ErrorText = styled.div`
  color: red;
  font-size: 12px;
  margin-top: 5px;
`;
