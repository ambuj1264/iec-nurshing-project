import { Flex } from '@chakra-ui/react'
import React from 'react'
import { inputStyle } from '../../eventbooking/tabsStep/Yourself'
import styled from 'styled-components'
import InputField from '../../list/ui/input/InputField'
import { FooterComponent, HeaderComponent } from '../Login'
import NumberWithCode from '../../list/ui/input/NumberWithCode'

const LoginId = ({ onLoginStep, formikProps,isEmail,prevStep}) => {
    const { values, errors,isValid,handleChange } = formikProps;
    return (
        <Wrapper>
            <HeaderComponent title="Login with I Elderly Care" subTitle="This won`t take long!" onclick={prevStep} />
            <Form>
                <Flex direction={{ base: "column", md: "row" }} alignItems="center" gap="15px" w="100%" mb={{ base: "20px", md: "40px" }}>
                    {isEmail ? <InputField style={inputStyle} type="email" placeholder="Continue with email" name="email" label="Email" value={values.email} helperText={errors.email} onChange={handleChange} />
                        : <NumberWithCode values={values} errors={errors} handleChange={handleChange} isSelect />}
                </Flex>
                <FooterComponent text="CONTINUE" onclick={onLoginStep} isValid={isValid} />
            </Form>
        </Wrapper>
    )
}

export default LoginId;

const Wrapper = styled.div``

const Form = styled.div`
 margin:30px 0
`