import React, { useState } from 'react'
import InitialStep from './loginSteps/InitialStep';
import LoginId from './loginSteps/LoginId';
import Verification from './loginSteps/Verification';
import ConfirmDetails from './loginSteps/ConfirmDetails';
import LoginSuccess from './loginSteps/LoginSuccess';
import { Form, Formik } from 'formik';
import styled from 'styled-components';
import { validateStep } from '../../utils/validation/ValidationSchema';
import { BackArrowIcon } from '../../utils/SVGIcons/SVG';

const Login = () => {
    const [loginStep, setLoginStep] = useState(0)
    const [loginType, setLoginType] = useState("Number")
    const handleLoginStep = () => {
        setLoginStep(loginStep + 1)
    }
    const handleLoginPrevStep = () => {
        setLoginStep(loginStep - 1)
    }
    const continueWithType = (value) => {
        setLoginType(value);
        handleLoginStep()
    }
    const isEmail = loginType === "Email"

    const renderStep = (formikProps) => {
        switch (loginStep) {
            case 0:
                return <InitialStep formikProps={formikProps} onLoginStep={continueWithType} nextStep={handleLoginStep} />;
            case 1:
                return <LoginId isEmail={isEmail} formikProps={formikProps} prevStep={handleLoginPrevStep}/>;
            case 2:
                return <Verification isEmail={isEmail} formikProps={formikProps} />;
            case 3:
                return <ConfirmDetails isEmail={isEmail} formikProps={formikProps} />;
            case 4:
                return <LoginSuccess />
            default:
                return null;
        }
    };

    return (
        <LoginWrapper>
            <Formik
                initialValues={{ code: "+91", email: "", name: "", number: "", otp1: "", otp2: "", otp3: "", otp4: "" }}
                validationSchema={() => validateStep(loginStep, isEmail)}
                onSubmit={(values, { setSubmitting }) => {
                    handleLoginStep();
                    setSubmitting(false);
                }}
            >
                {(formikProps) => (
                    <Form>
                        {renderStep(formikProps)}
                    </Form>
                )}
            </Formik>
        </LoginWrapper>
    )
}

export default Login;


export const HeaderComponent = ({ title, subTitle, onclick }) => {
    const handleClick=()=>{
        console.log("Sa");
        if(onclick)
        {
            onclick()

        }
    }
    return <>
        <Wrapper>
            {onclick && <BackArrowIcon onclick={handleClick} />}
            <Content>
                <Title>{title}</Title>
                {subTitle && <SubTitle>{subTitle}</SubTitle>}
            </Content>
        </Wrapper>
    </>
}


export const FooterComponent = ({ text, onclick ,isValid=false}) => {
    return <FooterWrapper>
        <StyledButton isValid={isValid} onClick={onclick}>{text}</StyledButton>
    </FooterWrapper>
}


const LoginWrapper = styled.div`
 min-height:500px;
`

const FooterWrapper = styled.div`
background: #FFF;
box-shadow: 0px -2px 4px 0px rgba(0, 0, 0, 0.08);
padding:10px;
position: absolute;
bottom: 0;
width: 100%;
left: 0;

`
const StyledButton = styled.button`
background:${props => (props.isValid ? '#0A84FF' : '#EEEEEE')};
color:${props => (props.isValid ? '#fff' : '#000')};
width:100%;
padding:10px;
border-radius:5px
`

const Wrapper = styled.div`
  margin:20px 0;
`;

const Content = styled.div`
 margin:15px 0
`

const Title = styled.h1`
color: #333;
font-size: 18px;
font-weight: 600;
margin:5px 0
`;

const SubTitle = styled.p`
color: #666;
font-size: 12px;
font-weight: 500;
line-height: 16px;
`