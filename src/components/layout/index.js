import { Box, Container } from "@chakra-ui/react";
import BannerSlider from '../home-carousal'
import Footer from "./footer";
import Header from "./header";

const Layout = ({ children, showBanner, showMenu = true }) => {
  return (
    <Box  backgroundColor={'#f3f3f3'}  width={'100%'}>
      <Header showMenu={showMenu}/>
      {showBanner && 
        <BannerSlider />
      }
      <Box as="main"  display={'flex'} justifyContent={'center'}  marginBlock={{md:10, base: 0}} minH="calc(100vh - 610px)">
          {children} 
      </Box>
      <Footer />
    </Box>
  );
};

export default Layout;