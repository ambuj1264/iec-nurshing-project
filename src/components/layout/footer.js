import { Box, Divider, GridItem, HStack, Icon, Image, Link, List, ListItem, SimpleGrid, Text } from "@chakra-ui/react";
import { relative } from "path";
import { BsYoutube } from "react-icons/bs";
import { TfiYoutube } from 'react-icons/tfi';
import { RiFacebookCircleLine } from 'react-icons/ri';
import { BsSnapchat, BsTwitter } from 'react-icons/bs';
import { ImInstagram } from 'react-icons/im';
const Footer = () => {
  return (
    <Box position={'relative'} overflow={'hidden'}>
      <Box
        position="absolute"
        width="80%"
        height="320px"
        left="-32%"
        top="29px"
        bg="#EFBFFF"
        opacity="0.6"
        clipPath="ellipse(25% 40% at 50% 50%)"
      ></Box>
      <Box
        position="absolute"
        width="100%"
        height="320px"
        right="10%"
        top="30px"
        bg="#EFBFFF"
        opacity="0.6"
        clipPath="ellipse(25% 40% at 50% 50%)"
      ></Box>
      <Box
        position="absolute"
        width="90%"
        height="320px"
        left="30%"
        top="29px"
        bg="#EFBFFF"
        opacity="0.6"
        clipPath="ellipse(25% 40% at 50% 50%)"
      ></Box>
      <Box
        position="absolute"
        width="80%"
        height="300px"
        right="-30%"
        top="20px"
        bg="#EFBFFF"
        opacity="0.3"
        transform="rotate(-20deg)"
        clipPath="ellipse(25% 40% at 50% 50%)"
      ></Box>
      <Box display={'flex'} position={'absolute'} zIndex={2} width={'100%'} justifyContent={'center'} top={'-1px'}>
        {/* <Image src="/images/footer/illustration-01.png"></Image> */}

      </Box>
      <Box
        bg="#742F8B"
        // h="500px"
        minH={'900px'}
        position="relative"
        paddingBottom={'200px'}
        zIndex="1"
        top="120px"
        clipPath="polygon(50% 10%, 100% 0, 100% 100%, 0 100%, 0 0)"
      >

        <Box position={'relative'} flexDir={'column'} color={'#fff'} alignItems={'center'} width={'100%'} display={'flex'} top={40}>

          <Box maxWidth={'7xl'}>
            <Image src="/images/footer/host-event.png"></Image>
          </Box>
          <Box display={'flex'} columnGap={5} width={'100%'} flexDir={'row'} maxWidth={'7xl'} marginBottom={'100px'}>
            <Box display={'flex'} flexDir={'column'} flex={2}>
              <Text fontSize={'16px'} fontWeight={'600'} lineHeight={'28px'}>Discover your city with iElderlyCare</Text>
              <SimpleGrid paddingBlock={'20px'} columns={4} columnGap={10} color={'#fff'}>
                <GridItem>
                  <List lineHeight={'30px'}>
                    <ListItem>Mumbai</ListItem>
                    <ListItem>Delhi</ListItem>
                    <ListItem>Bangalore</ListItem>
                    <ListItem>Hyderabad</ListItem>
                  </List>
                </GridItem>
                <GridItem>
                  <List lineHeight={'30px'}>
                    <ListItem>Chennai</ListItem>
                    <ListItem>Ahmedabad</ListItem>
                    <ListItem>Jaipur</ListItem>
                    <ListItem>Gurgaon</ListItem>
                  </List>
                </GridItem>
                <GridItem>
                  <List lineHeight={'30px'}>
                    <ListItem>Noida</ListItem>
                    <ListItem>Kolkata</ListItem>
                    <ListItem>Pune</ListItem>
                    <ListItem>Chandigarh</ListItem>
                  </List>
                </GridItem>
                <GridItem>
                  <List lineHeight={'30px'}>
                    <ListItem>Navi Mumbai</ListItem>
                    <ListItem>Lakhnow</ListItem>
                    <ListItem>Coimbatore</ListItem>
                    <ListItem>Surat</ListItem>
                  </List>
                </GridItem>
              </SimpleGrid>
              <Divider marginBlock={2} borderColor={'#9750AF'}></Divider>
              <SimpleGrid columns={2} gridTemplateColumns={'100px 1fr'} columnGap={10} marginBlock={5}>
                  <GridItem>
                      <Image src={'/images/footer/logo.png'}></Image>
                  </GridItem>
                  <GridItem>
                      <Link paddingRight={5}>
                        About Us
                      </Link>
                      <Link paddingRight={5}>
                        Care Knowledge
                      </Link>
                      <Link paddingRight={5}>
                        FAQs
                      </Link>
                      <Link paddingRight={5}>
                        Terms & Conditions 
                      </Link>
                      <Link paddingRight={5}>
                        Privacy policy
                      </Link>
                      <Link paddingRight={5}>
                        Contact Us
                      </Link>
                  </GridItem>
              </SimpleGrid>
              <Divider marginBlock={2} borderColor={'#9750AF'}></Divider>
              <SimpleGrid columns={2} gridTemplateColumns={'1fr 1fr'} columnGap={10} marginBlock={5}>
                  <GridItem>
                      <Text fontSize={'16px'} fontWeight={'500'} marginBottom={'10px'}>FOR EVENT ORGANIZERS</Text>
                      <Box fontSize={'14px'} lineHeight={'24px'}>
                        <Link paddingRight={5}>
                          Host your Event
                        </Link>
                        <Link paddingRight={5}>
                          Associate with us
                        </Link>
                      </Box>
                  </GridItem>
                  <GridItem>
                      <Link paddingRight={10}>
                        <Icon as={TfiYoutube} width={'35px'} height={'33px'}/>
                      </Link>
                      <Link paddingRight={10}>
                        <Icon as={RiFacebookCircleLine} width={'35px'} height={'33px'}/>
                      </Link> 
                      <Link paddingRight={10}>
                        <Icon as={ImInstagram} width={'35px'} height={'33px'}/>
                      </Link> 
                      <Link paddingRight={10}>
                        <Icon as={BsTwitter} width={'35px'} height={'33px'}/>
                      </Link> 
                      <Link paddingRight={10}>
                        <Icon as={BsSnapchat} width={'35px'} height={'33px'}/>
                      </Link> 

                  </GridItem>
              </SimpleGrid>
            </Box>
            <Box display={'flex'} flexDir={'column'} position={'relative'} flex={1}>
              <Box display={'flex'} position={'absolute'} justifyContent={'center'} zIndex={1} top={'-20px'} width={'100%'} alignContent={'center'}>
                <Image src={'/images/footer/app.png'} />
              </Box>
              <Box
                clipPath={'polygon(50% 0%, 100% 38%, 100% 100%, 0 100%, 0% 38%)'}
                background={'#541469'}
                height={'350px'}
                width={'100%'}
                position={'relative'}

              >
                <Box position={'relative'} flexDir={'column'} marginTop={'20px'} top={'150px'} display={'flex'} alignItems={'center'}>
                  <Text fontSize={'24px'} fontWeight={'600'}>
                    Download our
                  </Text>
                  <Text fontSize={'24px'} color={'#FFC20E'} fontWeight={'600'}>
                    i elderly care app
                  </Text>
                  <Box display={'flex'} justifySelf={'flex-end'} marginTop={'30px'} gap={5}>
                    <Image src={'/images/footer/apple.png'} cursor={'pointer'} />
                    <Image src={'/images/footer/android.png'} cursor={'pointer'} />
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
         
        </Box>
      </Box>
    </Box>

  );
};

export default Footer;