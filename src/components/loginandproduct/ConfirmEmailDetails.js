import { Box, Button, Flex, Heading, VStack } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import InputField from "../list/ui/input/InputField";
import { useFormik } from "formik";
import { validationSchema } from "../../utils/validation/ValidationSchema";
import { inputStyle } from "../eventbooking/tabsStep/TicketDetails";
import { labelStyle } from "./PhoneNumberLogin";

const ConfirmEmailDetails = () => {
  const { values, touched, errors, handleBlur, handleChange, handleSubmit } =
    useFormik({
      initialValues: {
        name: "",
        email: "",
        contry: "+91",
        number: "",
        tickettype: "Bronze",
      },
      validationSchema: validationSchema,
      onSubmit: (values) => {
        dispatch(ticketDetails(values));
      },
    });
  return (
    <VStack align="stretch" mx="auto" my={"10px"} height={"420px"}>
      <Heading aas="h5" size="xs">
        Confirm details
      </Heading>
      <form onSubmit={handleSubmit}>
        <Box mb={"15px"}>
          <InputField
            style={inputStyle}
            label="Name*"
            variant="flushed"
            labelStyle={labelStyle}
            placeholder="Please enter your name"
            name="name"
            type="text"
            value={values.name}
            helperText={touched.name && errors.name}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </Box>

        <Box mb={"25px"}>
          <InputField
            style={inputStyle}
            label="Email*"
            labelStyle={labelStyle}
            type="email"
            variant="flushed"
            placeholder="Please enter your email"
            name="email"
            value={values.email}
            helperText={touched.email && errors.email}
            onBlur={handleBlur}
            onChange={handleChange}
          />
          <Flex alignItems="end" justifyContent="end" color="blue">
            Terms & Conditions
          </Flex>
        </Box>
 
      </form>
      <Flex flex="1" justifyContent="center" alignItems="flex-end">
          <button
            style={{
              width: "384px",
              height: "36px",
              top: "492px",
              left: "511px",
              backgroundColor: "#0A84FF",
              color: "white",
            }}
          >
          Next
          </button>
        </Flex>
    </VStack>
  );
};

export default ConfirmEmailDetails;
