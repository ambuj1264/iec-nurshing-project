import { Button, Divider, LightMode } from "@chakra-ui/react";
import * as React from "react";
import styled from "styled-components";

function EditDetails(props) {
    return (
        <Div>
            <Span>
                <Div2>Other Details</Div2>
                <Span2>
                    <Div3>
                        mansuri.wasim@gmail.com
                        <br />
                        9930391551 | Maharashtra
                    </Div3>
                    <Div4>Edit</Div4>
                </Span2>
                <Span3>
                    <Div5>
                        Abdul Rashid Mansuri
                        <br />
                        9930391551 | Maharashtra
                    </Div5>
                    <Div6>Edit</Div6>
                </Span3>
           <Divider/>
                <Span4>
                    <Div7>
                        Parveen Mansuri
                        <br />
                        9930391551 | Maharashtra
                    </Div7>
                    <Div8>Edit</Div8>
                </Span4>
                <Divider/>
            
                <Span5>
               
                    <Div9>
                        By proceeding, I express my consent to compete this transaction
                    </Div9>
                </Span5>
            </Span>
            <ContinueButton>
                    <Button
                        type="submit"
                        my={5}
                        style={{
                            backgroundColor: "#0c8edd", color: "white", width: "209px",
                            height: "36px",
                            borderRadius:"8px"
                        }}
                    >
                        Continue
                    </Button>
                </ContinueButton>
        </Div>
    );
}
export default EditDetails;
const ContinueButton = styled.div`
display: flex;
justify-content: center;
align-items: center;
`
const Div = styled.div`
  display: flex;
  max-width: 262px;
  flex-direction: column;
`;

const Span = styled.span`
  display: flex;
  width: 100%;
  padding-left: 11px;
  flex-direction: column;
`;

const Div2 = styled.div`
  color: #333;
  text-transform: capitalize;
  font: 600 14px/157% Inter, sans-serif;
`;

const Span2 = styled.span`
  display: flex;
  margin-top: 19px;
  justify-content: space-between;
  gap: 20px;
`;

const Div3 = styled.div`
  color: #666;
  font: 400 12px/20px Inter, sans-serif;
`;

const Div4 = styled.div`
  color: var(--CTA, #0a84ff);
  text-transform: capitalize;
  align-self: start;
  font: 400 12px/167% Inter, sans-serif;
`;

const Img = styled.img`
  aspect-ratio: 251;
  object-fit: contain;
  object-position: center;
  width: 100%;
  stroke-width: 1px;
  stroke: #eee;
  overflow: hidden;
  margin-top: 15px;
`;

const Span3 = styled.span`
  display: flex;
  margin-top: 16px;
  justify-content: space-between;
  gap: 20px;
`;

const Div5 = styled.div`
  color: #666;
  font: 400 12px/20px Inter, sans-serif;
`;

const Div6 = styled.div`
  color: var(--CTA, #0a84ff);
  text-transform: capitalize;
  align-self: start;
  font: 400 12px/167% Inter, sans-serif;
`;

const Span4 = styled.span`
  display: flex;
  margin-top: 16px;
  justify-content: space-between;
  gap: 20px;
`;

const Div7 = styled.div`
  color: #666;
  font: 400 12px/20px Inter, sans-serif;
`;

const Div8 = styled.div`
  color: var(--CTA, #0a84ff);
  text-transform: capitalize;
  align-self: start;
  font: 400 12px/167% Inter, sans-serif;
`;

const Span5 = styled.span`
  display: flex;
  margin-top: 11px;
  align-items: start;
  justify-content: space-between;
  gap: 8px;
`;

const Img4 = styled.img`
  aspect-ratio: 1;
  object-fit: contain;
  object-position: center;
  width: 16px;
  overflow: hidden;
  max-width: 100%;
`;

const Div9 = styled.div`
  color: #666;
  margin-top: 4px;
  width: 100%;
  font: 400 11px/16px Inter, sans-serif;
`;

const Div10 = styled.div`
  disply: flex;
  text-transform: capitalize;
  position: relative;
  white-space: nowrap;
  fill: var(--CTA, #0a84ff);
  overflow: hidden;
  aspect-ratio: 7.277777777777778;
  margin-top: 39px;
  width: 100%;
  justify-content: center;
  align-items: center;

  font: 700 12px Inter, sans-serif;
`;

const Img5 = styled.img`
  position: absolute;
  inset: 0;
  height: 100%;
  width: 100%;
  object-fit: cover;
  object-position: center;
  z-index:-11;
`;

const Div11 = styled.div`
  position: relative;
`;


