import React from 'react'
import OrderDetails from './OrderDetails'
import styled from 'styled-components'
import EditDetails from './EditDetails'

const OrderSummary = () => {
  return (
    <>
    <Wrapper>
    <OrderDetails/>
    <EditDetails/>
    </Wrapper>
    </>
  )
}

export default OrderSummary
const Wrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;