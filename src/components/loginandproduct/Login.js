import { Center, Divider, Stack, Text, color } from "@chakra-ui/react";
import styled from "@emotion/styled";
import React, { useEffect, useState } from "react";
import {
  VStack,
  Box,
  FormLabel,
  Flex,
  Button,
  Checkbox,
  FormControl,
} from "@chakra-ui/react";
import { useFormik } from "formik";
import { validationForNumber } from "../../utils/validation/ValidationSchema";
import SelectBox from "../list/ui/input/SelectBox";
import { countryoptions } from "../../utils/constant";
import InputField from "../list/ui/input/InputField";
import { GoogleIcon } from "../../utils/SVGIcons/SVG";
import { EmailIcon } from "@chakra-ui/icons";
import VerifyMobile from "./VerifyMobile";
import PhoneNumberLogin from "./PhoneNumberLogin";
import EmailLogin from "./EmailLogin";
import { useSelector } from "react-redux";

const Login = () => {
  const loginType = useSelector((state) => state.loginTypeReducer);

  const LogingType = () => {
    switch (loginType.type) {
      case "MOBILE_NUMBER":
        return <PhoneNumberLogin />;
      case "MOBILE_OTP":
        return <VerifyMobile />;
      case "EMAIL_STEP_1":
        return <EmailLogin />;
      default:
        return <PhoneNumberLogin />;
    }
  };
  return (
    <>
      <Wrapper>
        {LogingType()}
        <Center height="150px">
          <Divider orientation="vertical" />
        </Center>
        <SocialLogin>
          <Stack spacing={3}>
            <Box width="100%" my={{ base: "5px", md: "10px" }}>
              <StyledButton px={{ base: "15px", md: "25px" }} py="25px">
                <GoogleIcon />
                <Text width="100%">Continue with Google</Text>
              </StyledButton>
              <StyledButton px={{ base: "15px", md: "25px" }} py="25px">
                <EmailIcon />
                <Text width="100%">Continue with Email</Text>
              </StyledButton>
            </Box>
          </Stack>
        </SocialLogin>
      </Wrapper>
    </>
  );
};

export default Login;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;
const PhoneLogin = styled.div``;
const SocialLogin = styled.div``;
const LoginMainHeading = styled.p`
  font-family: Inter;
  font-size: 16px;
  font-weight: 700;
  line-height: 19px;
  letter-spacing: 0em;
  text-align: left;
`;
const LoginSubHeading = styled.p`
  font-family: Inter;
  font-size: 12px;
  font-weight: 500;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
`;

export const labelStyle = {
  color: "#666",
  fontFamily: "Inter",
  fontSize: "12px",
  fontWeight: 500,
  lineHeight: "19px",
};
export const buttonStyle = {
  width: "100%",
  color: "#999999",
  bg: "transparent",
  border: "none",
  borderRadius: "8px",
  padding: "20px",
  hieght: "220px",
};
const StyledButton = styled(Button)`
  display: flex !important;
  justify-content: start !important;
  align-items: center !important;
  color: #333333 !important;
  font-size: 14px !important;
  font-weight: 500 !important;
  border-radius: 0 !important;
  border: 1px solid #eee !important;
  width: 100% !important;
  padding: ${({ theme }) => theme.px};
  margin-bottom: 15px !important;

  @media (min-width: 48em) {
    padding: ${({ theme }) => theme.pxMd};
  }
`;
